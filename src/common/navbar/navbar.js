(function () {
    'use strict';

    function NavbarController($scope, $window) {
        var self = this;
        angular.element($window).bind('scroll', function () {
            if (this.pageYOffset >= 70) {
                self.overlayNavbar = true;
            } else {
                self.overlayNavbar = false;
            }
            $scope.$apply();
        });

        self.isMobileMenuShown = false;
        self.toggleMobileMenu = function () {
            self.isMobileMenuShown = !self.isMobileMenuShown;
        }
    }

    function navbar() {
        return {
            controller: NavbarController,
            controllerAs: 'ctrl',
            replace: true,
            restrict: 'E',
            scope: {},
            templateUrl: 'common/navbar/navbar.html'
        };
    }

    angular.module('common.navbar', [])
        .directive('navbar', navbar);

}());