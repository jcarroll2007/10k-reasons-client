(function () {
    'use strict';

    angular.module('myAppName', [
        'api',
        'app',
        'common',
        'models',
        'ngAnimate',
        'templates'
    ]);

}());