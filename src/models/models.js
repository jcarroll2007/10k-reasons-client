(function () {
    'use strict';

    angular.module('models', [
        'models.submission'
    ]);
}());